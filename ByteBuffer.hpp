	//
	//  ByteBuffer.hpp
	//  s
	//
	//  Created by cn on 15-5-31.
	//  Copyright (c) 2015年 cnavy. All rights reserved.
	//
#pragma once
#include <memory>
#include <string>
#include <vector>
#include <map>

#include <list>
#include <set>
#include <assert.h>
#include <iostream>
#include <fstream>

#include <stdio.h>

using namespace std;

typedef signed char         int8;
typedef short               int16;
typedef int                 int32;
typedef long	        	int64;

typedef unsigned char       uint8;
typedef unsigned short      uint16;
typedef unsigned int        uint32;
typedef unsigned long		uint64;

typedef int8  i8;
typedef int16 i16;
typedef int32 i32;
typedef int64 i64;

typedef uint8  u8;
typedef uint16 u16;
typedef uint32 u32;
typedef uint64 u64;

const int DEFAULT_SIZE = 4096;

class ByteBuffer {

protected:
	std::vector<uint8> _buffer;
	uint32 _rpos, _wpos;

public:
	ByteBuffer()
	:_rpos(0)
	, _wpos(0)
	{
		*this << _wpos;
	}

		// ==================== write =====================
	void clean()
	{
		_wpos = _rpos = 0;
		_buffer.clear();
	}
	void resize(uint32 size)
	{
			//		cout << "resize buffer " << &_buffer << ": " << _buffer.size() << " to " << size << endl;
		assert(size <= 1024*1024*32);//"buffer largger than 32MB"
//		_buffer.resize(size+64);
		_buffer.resize(size);
	}
	void setHeader()
	{
		assert(_wpos >= 4);
			//		cout << "setHeader " << _wpos << ", buf size: " << _buffer.size() << endl;
		memcpy(&_buffer[0], (char*)&_wpos, 4);
	}

	ByteBuffer& operator<<(const char* s)
	{
		uint32 size = strlen(s)+1;
		if (_wpos + size + sizeof(size) > _buffer.size())
			resize(_wpos + size  + sizeof(size));

		*this << size;
#ifdef DEBUG
		cout << "serilise an [string]:" << s << endl;
#endif
		memcpy(&_buffer[_wpos], s, size);
		_wpos += size;
		setHeader();
		return *this;
	}

	ByteBuffer& operator<<(char* s)
	{
		return *this << (const char*)s;
	}

	ByteBuffer& operator<<(string& s)
	{
		return *this << s.c_str();
	}

	ByteBuffer& operator<<(string&& s)
	{
		return *this << s.c_str();
	}

	template<typename T>
	ByteBuffer& operator<<(vector<T>& v)
	{
		uint32 size = v.size();
		if(size > 0)
		{
			if (_wpos + size * sizeof(T) + sizeof(size)> _buffer.size())
				resize(_wpos + size * sizeof(T) + sizeof(size));
			*this << size;
			for (typename vector<T>::iterator i = v.begin(); i != v.end(); ++i)
			{
				*this << *i;
			}
		}
		setHeader();
		return *this;
	}

	template<typename TK, typename TV>
	ByteBuffer& operator<<( std::map<TK, TV>& v )
	{
		uint32 size = v.size();
		if(size > 0)
		{
			if (_wpos + size * ( sizeof(TK) + sizeof(TV) ) + sizeof(size )> _buffer.size())
				resize( _wpos + size * ( sizeof(TK) + sizeof(TV) ) + sizeof(size) );
			*this << size;
			for (typename std::map<TK, TV>::iterator i = v.begin(); i != v.end(); ++i)
			{
				*this << i->first << i->second;
			}
		}
		setHeader();
		return *this;
	}

	template<typename T>
	ByteBuffer& operator<<(T* v)
	{
#ifdef DEBUG //for debug output
		cout << "*serilise an [" << typeid(T*).name() << "]: ";
		switch(typeid(T).name()[0])//[Pi,Pd,Pl,Pm,Ps...]
		{
			case 'i':
			case 'd':
			case 'l':
			case 'm':
			case 's':
			case 'y':
				cout << &*v;
				break;
			default:
				cout.write((const char*)v, sizeof(T));
				break;
		}
		cout << endl;
#endif
		return *this << *v;
	}
		//	//<==>
		//	template<typename T> //don't use this
		//	ByteBuffer& operator<<(T&& v)
		//	{
		//		*this << v;
		//	}

	template<typename T>
	ByteBuffer& operator<<(T v)
	{
		uint32 size = sizeof(T);
		if (_wpos + size > _buffer.size())
			resize(_wpos + size);
		memcpy(&_buffer[_wpos], (char *)&v, size);
		_wpos += size;

#ifdef DEBUG //for debug output
		cout << "serilise an [" << typeid(T).name() << "]: ";
		switch(typeid(T).name()[0])
		{
			case 'i':
				cout  << *(int*)&v;
				break;
			case 'j':
				cout  << hex << *(uint32*)&v << dec;
				break;
			case 'd':
				cout << *(double*)&v;
				break;
			case 'l':
				cout << hex  << *(long*)&v;
				break;
			case 'y':
			case 'm':
				cout << *(size_t*)&v;
				break;
			case 's':
				cout << *(short*)&v;
				break;
			default:
				cout.write((const char*)&v, sizeof(v));
				break;
		}
		cout << endl;
#endif
		setHeader();
		return *this;
	}

	bool saveTo(const char* filename)
	{
		cout << "fsave to file: " << filename << endl;
		FILE* file = fopen(filename, "wb");
		if( file == NULL)
			return false;
		fwrite(&_buffer[0], _wpos, 1, file);
		fclose(file);
		return true;
	}

	bool readFrom(const char* filename)
	{
		FILE* file = fopen(filename, "rb");
		if( file == NULL)
			return false;
		fread((char*)&_wpos, sizeof(uint32), 1, file);

		if (_wpos > _buffer.size())
			resize(_wpos);
		fread(&_buffer[0], sizeof(uint8), _wpos, file);
		fclose(file);

		return true;
	}
		// ==================== read =====================
	template<typename T>
	T read()
	{
		assert(_rpos + sizeof(T) <= _wpos);
		T v = T();
		memcpy((void*)&v, &_buffer[_rpos], sizeof(T));
		_rpos += sizeof(T);
#ifdef DEBUG //for debug output
		cout << "read an [" << typeid(T).name() << "]: ";
		switch(typeid(T).name()[0])
		{
			case 'i':
				cout << *(int*)&v;
				break;
			case 'j':
				cout << hex << *(uint32*)&v << dec;
				break;
			case 'd':
				cout << *(double*)&v;
				break;
			case 'l':
				cout << *(long*)&v;
				break;
			case 'y':
			case 'm':
				cout << *(size_t*)&v;
				break;
			case 's':
				cout << *(short*)&v;
				break;
			default:
				cout.write((const char*)&v, sizeof(v));
				break;
		}
		cout << endl;
#endif
		return v;
	}

	ByteBuffer& operator>>(string& s)
	{
		assert(_rpos < _wpos);
		uint32 size = read<uint32>();
		assert(_rpos + size <= _wpos);
		if(s.size()<size)
			s.resize(size);
		memcpy(&*s.begin(), &_buffer[_rpos], size);
		_rpos += size;

		return *this;
	}

	ByteBuffer& operator>>(char* s)
	{
		assert(_rpos < _wpos);
		uint32 size = read<uint32>();
		assert(_rpos + size <= _wpos);
		memcpy(s, &_buffer[_rpos], size);
		_rpos += size;
		return *this;
	}

	template<typename T>
	ByteBuffer& operator>>(vector<T>& v)
	{
		assert(_rpos < _wpos);
		uint32 size = read<uint32>();
		T t;
		for (int i = 0; i < size; ++i)
		{
			*this >> t;
			v.push_back(t);
		}
		return *this;
	}

	template<typename TK, typename TV>
	ByteBuffer& operator>>(std::map<TK, TV>& m)
	{
		assert(_rpos < _wpos);
		uint32 size = read<uint32>();
		TK k;
		TV v;
		for (int i = 0; i < size; ++i)
		{
			*this >> k >> v;
			m[k] = v;
		}
		return *this;
	}

	template< typename T>
	ByteBuffer& operator>>(T& v)
	{
		v = read<T>();
		return *this;
	}
};
