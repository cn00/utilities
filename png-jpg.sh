#!/bin/bash
# convert jpeg & png files
# usage: jpg2png <file>

format=""
function parserArg(){
    if [ $1 == "p" ];then
        format="png" 
    elif [ $1 == "j" ];then
        format="jpg"
    else
        echo "wrong convert format"
        exit 0
    fi

}

if [[ $# < 1 ]] ; then
    echo "USAGE: $0 <options>:[p, j]"
    echo -en "chose convert type \033[0;94;3m[p:png2jpg, j:jpg2png]\033[0;0;5m (p/j)\033[0m: "
    read char
    parserArg $char
else
    parserArg $1 
fi

mkdir out
for f in *.${format}
do
    if [ $format == jpg ];then
        sips -s format png --out out/${f%.*}.png $f
    elif [ $format == png ];then
        sips -s format jpeg --out out/${f%.*}.jpg $f
    fi

done


