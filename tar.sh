#!/usr/bin/sh
# target="trunk-`date -Imin | awk -F '+' '{print $1}' | sed -e s/[,:T+\ ]/_/g`.zip"
#coding=utf-8

target="trunk-3.4"
uploadip="172.16.0.98"
uploaddir="Hero/Pc"

dir=$0
echo $0
cd ${dir%\/*}

function gettime(){
	f=$1
	d=`stat -c "%y" $f`
	d=${d//" +0800"/""}
	d=${d/"."/"_"}
	d=${d//":"/"_"}
	d=${d//" "/"-"}
	echo $d
}

svn cleanup ../../data
svn up ../../data

echo "tar:"$target".zip..."
# if [[ ! -f "${target}.zip" ]]; then
   #tar -ahczf $target.zip *.dll *.exe *.png *.json data Document/* -x "*/[._]*"
	zip -r     $target.zip *.dll *.exe *.png *.json data Document/* -x "*/[._]*"
# else
# 	tar -ahuzf ${target}.zip *.dll *.exe *.png *.json data Document/* --exclude="data/.svn" --exclude="data/*.txt"
# fi

# rename
temp=$target"-`gettime client_exe.exe`.zip"
mv $target.zip $temp

# upload
# EOF之间不可写注释
# prompt
# mdelete "trunk-*.zip"
# mkdir old
# rename "trunk-*.zip" old/$0
ftp -nv 172.16.0.98 << EOF
user d9user d9user
cd Hero/Pc/
bin
put $temp
bye
EOF

# echo "remove local zip: "$target
# rm  $target
mv $temp $target.zip
echo -e "upload \033[0;31;35m[ftp://${uploadip}/${uploaddir}/${temp}]\033[0m done"

echo "commit svn ..."
svn ci . -m "update pc client `date "+%Y-%m-%d-%H%M%S"`"

chmod +x *.exe