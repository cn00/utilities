#!/bin/bash

set -e

# only for unix/linux/mac

echo -e " ====== Jenkins param ======\n ACT_BUILD_TYPE:${ACT_BUILD_TYPE}\n ACT_CHANNEL:${ACT_CHANNEL}\n ACT_GEN_VERSION:${ACT_GEN_VERSION}\n ACT_BUILD_CONFIGURATION:${ACT_BUILD_CONFIGURATION}\n ====== Jenkins param end ======"

svn='/usr/bin/svn'
svnhead=$(${svn} info | grep 'Last Changed Rev: ' | cut -d ':' -f 2 | cut -d ' ' -f 2)

uploadip="${ACT_UPLOAD_FTP/:*/}"
uploaddir="${ACT_UPLOAD_FTP/*:/}"

if [[ ${ACT_BUILD_TYPE} == 'GEN_UPDATE' ]];then
	echo 'gen GEN_UPDATE'
    exit 0
fi

export LANG=en_US.UTF-8

# version=`cat .version|awk -F= '{print $2}'`
alltargets=$(ls frameworks/runtime-src/proj.android/cfg)
if [[ ${ACT_CHANNEL} == 'all' ]] ; then
	targets="${alltargets}"
else
	targets="${ACT_CHANNEL}"
fi

echo -e " ========== active targets ========== \n${targets} \n"
echo -e " ========== available targets ========== \n${alltargets} \n"

mkdir -p "apks/${ACT_GEN_VERSION}"
build(){
	cp -rf "frameworks/runtime-src/proj.android/cfg/$1/" "frameworks/runtime-src/proj.android/"
	cocos compile -p android -m $(tr '[A-Z]' '[a-z]' <<< "${ACT_BUILD_CONFIGURATION}")
    targetapk="actgame_$1-${ACT_BUILD_CONFIGURATION}-${ACT_GEN_VERSION}-r${svnhead}-$(date '+%Y%m%d_%H%M%S').apk"
	mv "frameworks/runtime-src/proj.android/bin/ActGame-${ACT_BUILD_CONFIGURATION}.apk" "apks/${ACT_GEN_VERSION}/$targetapk"
	echo "uploading: [ftp://${uploadip}/${uploaddir}/${ACT_GEN_VERSION}/apk/${targetapk}] ... "
	pushd "apks/${ACT_GEN_VERSION}/"
	
	ftp -nv ${uploadip} << EOF
user anonymous cool_navy@126.com
bin
cd ${uploaddir}
mkdir ${ACT_GEN_VERSION}
mkdir ${ACT_GEN_VERSION}/apk
cd    ${ACT_GEN_VERSION}/apk
put "${targetapk}"
bye
EOF
	popd
	echo "upload to  [ftp://${uploadip}/${uploaddir}/${ACT_GEN_VERSION}/apk/${targetapk}]  done "

}

for target in $targets; do
	build $target
done

#allapks=`find apks -name "*.apk"`
#sed -e '/^<dev id=\"apks\">/,/^<\/dev>/d' -i 'index.html'
#sed -e '/<\!-- apks -->/a\<dev id=\"apks\">\n<\/dev>' -i 'index.html'
#for apk in  $allapks; do
#	sed -e '/<dev id=\"apks\">/a\\t<p><a href=\"'$apk'\"><img src=\"Icon-120.png\" title=\"魔法少女小圆\"><br\/>'${apk/*\//}'<\/a><\/p>' -i "index.html"
#done
