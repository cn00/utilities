#!/bin/bash
# gen & upload ipa

# ftp
uploadip="192.168.10.124"
uploaddir="ACT"

# config
v="1.1.0"
# == it seemd that xctool can only catch single arch ==
#buildtool="xctool"
xcodeprojdir="proj.ios"
buildtool="xcodebuild"
configuration="Release"
#configuration="Debug"
sdk=`xcodebuild -showsdks | grep -e "^\siOS" | sed -e 's/^.*-sdk/-sdk/'`
#archs="-arch armv7"
archs="-arch arm64 -arch armv7"

targets=()
if [[ $# < 1 ]] ; then
	#targets=("ActGame iOS" "ActGame Mac")
	targets=("ActGame iOS")
    #armv7 & arm64
	#targets=("client_app111" "client_changba" "client_d9" "client_d9_appstore" "client_xy")
    echo "USAGE: $0 [<target1> <target2> ... ]"
    echo "      empty target, build all these targets below? ^v^" 
    echo -e "\033[0;31;1m      ${targets[@]}\033[0m"
    echo -n "input (y) to continue: "
    read char
    if [ $char != "y" ];then
        exit 0
    fi
else
	targets="$@"
fi

#svn up .
#svn up data
#svn up data_rc4
#rm -rf data_rc4/data/
#./rc4
#svn ci data_rc4 -m "merge from data `date "+%Y-%m-%d-%H%M%S"`"

# check target
cd ${xcodeprojdir}
targetlist=`xcodebuild -list | grep client_`
echo "=================== active targets ======================="
#for target in ${targets[@]}
#do
#	echo $target
#done
echo -e "\033[0;31;35m${targets[@]}\033[0m"
echo "================== available targets ====================="
echo $targetlist
echo "=========================================================="
#exit 1

# confirm targets
#echo -e "\033[0;31;35 confirm those targets \033[0m"
#echo -n "input (y) to continue: "
#read char
#if [ $char != "y" ];then
#    exit 0
#fi

function renameIpa(){
	f=$1
	d=$(stat -xt "%Y%m%d-%H%M%S" $f | grep "Modify: ")
	d=${d#"Modify: "}
	n=${f//"client"/"hero"}
	n=${n//.ipa/-${configuration}-$v-$d.ipa}
	mv $f $n
	echo "$n"
}

#build
for target in ${targets[@]}
do
	#target=$1
	#if [[ $2 -eq "clean" ]]
	#	xcodebuild -scheme ${target} clean
	#fi
	${buildtool} -target ${target} -jobs 4 ${archs} ${sdk} -configuration ${configuration}  CONFIGURATION_BUILD_DIR=build/

	cd build
	xcrun --sdk iphoneos PackageApplication -v ${target}.app -o `pwd`/${target}.ipa

	#rename ipa
	n=`renameIpa ${target}.ipa`

	# echo "tar $n.dSYM.zip..."
	# tar -czf $n.dSYM.zip ${target}.app.dSYM
	echo "done"

# # upload ftp, EOF之间不能写注释
# # bi[nary] //使用二进制传输，默认为ascii
# ftp -nv ${uploadip} << EOF
# user d9user d9user
# bin
# cd ${uploaddir}
# mkdir $v
# mkdir $v/ios
# cd $v/ios
# put $n
# mkdir dsym
# cd dsym
# put $n.dSYM.zip
# bye
# EOF

scp "cn@${uploadip}/${uploaddir}/" "$n"

echo "upload done"

	cd ..
	#echo "clean local $n..."
	#rm $n
	#rm $n.dSYM.zip

#if [[ ${buildtool} -eq "xcodebuild" ]]
	osascript -e 'display notification "upload '$n' ok!!!" with title "'${target}'"'
#fi
	echo -e "upload to \033[0;31;35m[ftp://${uploadip}/${uploaddir}/$v/ios/$n]\033[0m done "

done

