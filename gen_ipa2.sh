# !/bin/bash

set -e

echo " ====== Jenkins param ======\n ACT_BUILD_TYPE:${ACT_BUILD_TYPE}\n ACT_CHANNEL:${ACT_CHANNEL}\n ACT_GEN_VERSION:${ACT_GEN_VERSION}\n ACT_BUILD_CONFIGURATION:${ACT_BUILD_CONFIGURATION}\n ====== Jenkins param end ======"

svn='/usr/bin/svn'

svnhead=$(${svn} info | grep 'Last Changed Rev: ' | cut -d ':' -f 2 | cut -d ' ' -f 2)

if [[ $(uname) == 'Darwin' ]]; then
	sed -e "s/^SVN_VERSION\s*=.*$/SVN_VERSION=\""${svnhead}"\"/g" -e "s/^VERSION\s*=\s*.*$/VERSION=\""${ACT_GEN_VERSION}"\"/" -i '' 'src/config.lua'
else
	sed -e "s/^SVN_VERSION\s*=.*$/SVN_VERSION=\""${svnhead}"\"/g" -e "s/^VERSION\s*=\s*.*$/VERSION=\""${ACT_GEN_VERSION}"\"/" -i 'src/config.lua'
fi

if [[ ${ACT_BUILD_TYPE} == 'GEN_UPDATE' ]];then
	echo 'gen GEN_UPDATE'
	"./gen_update_svn.sh" "${ACT_GEN_VERSION}"
    exit 0
fi

# config
## gen & upload ipa
## ftp
uploadip="${ACT_UPLOAD_FTP/:*/}"
uploaddir="${ACT_UPLOAD_FTP/*:/}"
# == it seemd that xctool can only catch single arch ==
#buildtool="xctool"
xcodeprojdir="."
buildtool="xcodebuild"
sdk=$(xcodebuild -showsdks | grep -e "^\siOS" | sed -e 's/^.*-sdk/-sdk/')
#archs="-arch armv7"
archs="-arch arm64 -arch armv7"

pushd "frameworks/runtime-src/proj.ios_mac"
targets=()

alltargets=$(xcodebuild -list | grep ActGame_ | sort -u | sed -e "s/^        //g")
if [[ ${ACT_CHANNEL} == 'all' ]] ; then
	targets=${alltargets}
else
	targets="ActGame_${ACT_CHANNEL}"
fi

echo -e " ========== active targets ========== \n${targets} \n\n"
echo -e " ========== available targets ========== \n${alltargets} \n\n"

function renameIpa(){
	f=$1
	d=$(stat -xt "%Y%m%d-%H%M%S" $f | grep "Modify: ")
	d=${d#"Modify: "}
	n=${f//"client"/"hero"}
	n=${n//.ipa/-${ACT_BUILD_CONFIGURATION}-${ACT_GEN_VERSION}-r${svnhead}-$d.ipa}
	mv $f $n
	echo "$n"
}

#build
for target in ${targets[@]};do
	#target=$1
	#if [[ $2 -eq "clean" ]]
	#	xcodebuild -scheme ${target} clean
	#fi
	${buildtool} -target "${target}" -jobs 4 ${archs} ${sdk} -configuration ${ACT_BUILD_CONFIGURATION}  CONFIGURATION_BUILD_DIR=build/
	# cocos compile -p ios -t "${target}" -m  ${ACT_BUILD_CONFIGURATION}
	
	cd build
	security unlock-keychain -pcvcv  "/Users/cn/Library/Keychains/login.keychain"
	xcrun --sdk iphoneos PackageApplication -v "${target}.app" -o "$(pwd)/${target}.ipa" # --sign "iPhone Distribution: Emile Technology Co., Ltd."
	#rename ipa
	n=$(renameIpa ${target}.ipa)
	# echo "tar $n.dSYM.zip..."
	# tar -czf $n.dSYM.zip ${target}.app.dSYM
	echo "done"
	## upload ftp, EOF
	## bi[nary] || ascii
	## ftp -nv address << EOF
	## user username passwd
	ftp -nv ${uploadip} << EOF
user anonymous cool_navy@126.com
bin
cd ${uploaddir}
mkdir ${ACT_GEN_VERSION}
mkdir ${ACT_GEN_VERSION}/ios
cd ${ACT_GEN_VERSION}/ios
put $n
bye
EOF
# mkdir dsym
# cd dsym
# put $n.dSYM.zip
	echo "upload to [ftp://${uploadip}/${uploaddir}/${ACT_GEN_VERSION}/ios/$n] done "
done

popd ## frameworks/runtime-src/proj.ios_mac 

echo "$svnhead=${ACT_GEN_VERSION}" > '.update_history'
echo "$svnhead=${ACT_GEN_VERSION}" > '.version'
